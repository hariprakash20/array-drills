const filter = require('../filter.cjs');

console.log(filter([1, 2, 3, 4, 5, 6], function(num){ return num % 2 == 0; }));

let cb = (num) =>{ return num % 2 == 0; }

console.log(filter(null,cb));

console.log(filter(undefined,cb));

console.log(filter(7459,cb));

console.log(filter(items,obj))