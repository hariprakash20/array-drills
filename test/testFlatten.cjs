const flatten = require('../flatten.cjs');

console.log(flatten([1, [2], [3, [[4]]]]));

console.log(flatten(null));

console.log(flatten(undefined));

console.log(flatten(""));