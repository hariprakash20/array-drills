const reduce = require('../reduce.cjs');

let obj = {one:1,two:2,three:3};

console.log(reduce([1, 2, 3], (sum, num)=>{ return sum + num; }))

let cb = (sum, num)=>{ return sum + num; };

reduce(null,cb);

reduce(undefined,cb);

console.log(reduce(7459,cb));

console.log(reduce(items,obj));