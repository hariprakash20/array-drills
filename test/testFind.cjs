const find = require('../find.cjs');

console.log(find([1, 2, 3, 4, 5, 6],(num)=>{ return num % 2 == 0; }));

console.log(find("Hello world"));

console.log(find(null));

console.log(find(undefined));

console.log(find(7459));

console.log(find(obj))