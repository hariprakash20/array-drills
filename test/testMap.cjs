const map = require('../map.cjs');

console.log(map([1, 2, 3],(num)=>{ return num * 3; }));

const items = [1, 2, 3, 4, 5, 5]; 
let cb = (num) => {return num*2};

let obj = {one:1,two:2,three:3};

console.log(map(items,cb));

console.log(map(null,cb));

console.log(map(undefined,cb));

console.log(map(7459,cb));

console.log(map(items,obj));

