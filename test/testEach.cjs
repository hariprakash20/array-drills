const each = require('../each.cjs');

const items = [1, 2, 3, 4, 5, 5]; 

const alert = (value, index, list) => {
    console.log(value + " in index " + index + " from " +list+ " is passed") ;
}

let obj = {one:1,two:2,three:3};

each(items,alert);

each([1, 2, 3], alert);

each({one:1,two:2,three:3},alert);

console.log(each(null,alert));

console.log(each(undefined,alert));

console.log(each(7459,alert));

console.log(each(items,obj))