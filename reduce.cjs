function reduce(elements, callback) {
    if(elements === undefined || elements === null){
        console.error("Cannot read properties of " + elements);
        return;
    }
    if(typeof callback !== 'function'){
        console.error('iteratee is not a function')
    }
    let result = 0;
    for(let element in elements){
            result = callback(result, elements[element]);
    }
    return result;
}

module.exports = reduce;