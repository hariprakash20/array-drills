function map(elements, callback) {
    if(elements === undefined || elements === null || typeof elements === 'number'){
        return [];
    }
    if(typeof callback !== 'function'){
        console.error('iteratee is not a function')
    }
    array = [];
    for(let element in elements){
        array[element] = callback(elements[element])
    }
    return array;
}

module.exports = map;