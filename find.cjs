function find(elements, callback) {
    for(let element in elements){
        if(callback(elements[element])){
            return elements[element];
        }
    }
    return undefined;
}

module.exports = find;

arr= [1,2,3];