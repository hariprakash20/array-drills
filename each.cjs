function each(elements, callback) {
    if(elements === undefined || elements === null || typeof elements === 'number'){
        return elements
    }
    if(typeof callback !== 'function'){
        console.error('iteratee is not a function')
    }
    let array = [];
    for(let element in elements){
        array[element] = callback(elements[element],element,elements)
    }
}

module.exports = each;