function flatten(elements) {
    if(elements === undefined || elements === null || typeof elements === 'number'){
        return elements
    }
    let array = [];
    for(let element in elements){
        if(Array.isArray(elements[element])){
            array = array.concat(flatten(elements[element]));
        }
        else{
            array.push(elements[element]);  
        }
    }
    return array;
}

module.exports = flatten