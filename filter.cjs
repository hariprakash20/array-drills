function filter(elements, callback) {
    if(elements === undefined || elements === null || typeof elements === 'number'){
        return elements
    }
    let array = [];
    for(let element in elements){
        if(callback(elements[element])){
             array.push(elements[element]);
        }
    }
    return array;
}
module.exports = filter;